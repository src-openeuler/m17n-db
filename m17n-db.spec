Name:          m17n-db
Version:       1.8.9
Release:       1
Summary:       Multilingual data files for the m17n library
License:       LGPL-2.1-or-later
URL:           http://www.nongnu.org/m17n
Source0:       https://download.savannah.nongnu.org/releases/m17n/%{name}-%{version}.tar.gz
Source1:       https://raw.githubusercontent.com/gnuman/m17n-inglish-mims/master/minglish/minglish.mim
BuildRequires: gettext glibc-locale-source gcc
BuildArch:     noarch
Requires:      (ibus-typing-booster >= 2.26.11 if ibus-typing-booster)
Requires:      (ibus-m17n >= 1.4.34 if ibus-m17n)

Obsoletes:     m17n-contrib < 1.1.14-4.fc20
Provides:      m17n-contrib = 1.1.14-4.fc20

%description
This package provides various multilingual datafiles to m17n library.
the m17n library describe input maps, encoding maps, OpenType font data and
font layout text rendering for languages.

%package       extras
Summary:       Extra data files for m17n-db
Requires:      m17n-db = %{version}-%{release}

Obsoletes:     m17n-contrib-extras < 1.1.14-4.fc20
Provides:      m17n-contrib-extras = 1.1.14-4.fc20

%description extras
Extra files of the m17n-db for input maps that are less used.

%package       devel
Summary:       Development files of the m17n-db
Requires:      m17n-db = %{version}-%{release}

%description devel
Development data files for m17n-db.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install

install -p %{S:1} %{buildroot}%{_datadir}/m17n

%find_lang m17n-db

%files
%doc AUTHORS README
%license COPYING
%dir %{_datadir}/m17n
%{_datadir}/m17n/mdb.dir
%{_datadir}/m17n/*.tbl
%{_datadir}/m17n/scripts
%{_datadir}/m17n/*.flt
# keymaps
%{_datadir}/m17n/a*.mim
%{_datadir}/m17n/b*.mim
%{_datadir}/m17n/c*.mim
%{_datadir}/m17n/d*.mim
%{_datadir}/m17n/e*.mim
%{_datadir}/m17n/f*.mim
%{_datadir}/m17n/g*.mim
%{_datadir}/m17n/h*.mim
%{_datadir}/m17n/i*.mim
%{_datadir}/m17n/k*.mim
%{_datadir}/m17n/l*.mim
%{_datadir}/m17n/m*.mim
%{_datadir}/m17n/n*.mim
%{_datadir}/m17n/o*.mim
%{_datadir}/m17n/p*.mim
%{_datadir}/m17n/r*.mim
%{_datadir}/m17n/s*.mim
%{_datadir}/m17n/t*.mim
%{_datadir}/m17n/u*.mim
%{_datadir}/m17n/v*.mim
%{_datadir}/m17n/y*.mim
# icons for keymaps
%dir %{_datadir}/m17n/icons
%{_datadir}/m17n/icons/*.png
%exclude %{_datadir}/m17n/zh-*.mim
%exclude %{_datadir}/m17n/icons/zh*.png
%exclude %{_datadir}/m17n/ja-*.mim
%exclude %{_datadir}/m17n/icons/ja*.png

%files extras -f m17n-db.lang
%{_datadir}/m17n/zh-*.mim
%{_datadir}/m17n/icons/zh*.png
%{_datadir}/m17n/ja*.mim
%{_datadir}/m17n/icons/ja*.png
%{_datadir}/m17n/*.fst
%{_datadir}/m17n/*.map
%{_datadir}/m17n/*.tab
%{_datadir}/m17n/*.lnm
%{_datadir}/m17n/LOCALE.*

%files devel
%{_bindir}/m17n-db
%{_datadir}/pkgconfig/m17n-db.pc

%changelog
* Thu Dec 12 2024 Funda Wang <fundawang@yeah.net> - 1.8.9-1
- update to 1.8.9
- hardcode downstream package version as this package contains non-ASCII input
  (https://github.com/mike-fabian/ibus-typing-booster/issues/537)

* Tue Oct 10 2023 yaoxin <yao_xin001@hoperun.com> - 1.8.4-1
- Upgrade to 1.8.4

* Wed Jun 09 2021 wulei <wulei80@huawei.com> - 1.8.0-6
- fixes failed: error: no acceptable C compiler found in PATH

* Sat Nov 30 2019 tianfei<tianfei16@huawei.com> - 1.8.0-5
- Package init

